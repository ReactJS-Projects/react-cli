// 将对象转换成字符串
const serialize = require('serialize-javascript')
const asyncBootstrap = require('react-async-bootstrapper').default
const ejs = require('ejs')
const ReactDomServer = require('react-dom/server')
const Helmet = require('react-helmet').default

const getStoreState = (stores) => {
  return Object.keys(stores).reduce((result, storeName) => {
    result[storeName] = stores[storeName].toJson()
    return result
  }, {})
}

module.exports = (bundle, template, req, res) => {
  return new Promise((resolve, reject) => {
    // 视频中是直接bundle点函数，但是在实际代码中报createStoreMap未定义，
    // 通过查看之前没有分离的代码知道需要bundle.exports点函数
    const createStoreMap = bundle.createStoreMap
    const createApp = bundle.default
    // const createStoreMap = bundle.exports.createStoreMap
    // const createApp = bundle.exports.default

    const routerContext = {}
    const stores = createStoreMap()
    const app = createApp(stores, routerContext, req.url)

    asyncBootstrap(app).then(() => {
      if (routerContext.url) {
        res.status(302).setHeader('Location', routerContext.url) // 302重定向
        res.end()
        return
      }
      const helmet = Helmet.rewind()
      const state = getStoreState(stores)
      const content = ReactDomServer.renderToString(app)

      const html = ejs.render(template, {
        appString: content,
        initialState: serialize(state),
        meta: helmet.meta.toString(),
        title: helmet.title.toString(),
        link: helmet.link.toString(),
        style: helmet.style.toString()
      })
      res.send(html)
      resolve()
    }).catch(reject)
  })
}
