// 应用入口
import React from 'react'
import { hydrate } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'mobx-react'
import { AppContainer } from 'react-hot-loader'  // eslint-disable-line
import App from './views/App'

import AppState from './store/app-state'

const initialState = window.__INITIAL__STATE__ || {} //eslint-disable-line
const root = document.querySelector('#root');
const render = (Component) => {
  // 如果使用服务端渲染需要使用hydrate方法，去渲染客户端的内容然。
  // 因为react会对比服务端生成的代码和客户端生成的代码之间的差别，
  // 如果有差别它会用客户端的代码覆盖服务端的代码
  hydrate(
    <AppContainer>
      <Provider appState={new AppState(initialState.appState)}>
        <BrowserRouter>
          <Component />
        </BrowserRouter>
      </Provider>
    </AppContainer>,
    root,
  )
}

render(App)

if (module.hot) {
  module.hot.accept('./views/App', () => {
    const NextApp = require('./views/App').default // eslint-disable-line
    render(App)
  })
}
